# XDG_BASE_DIRECTORY
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_DATA_HOME="${HOME}/.local/share"

# X11
export XINITRC="${XDG_CONFIG_HOME}/X11/xinitrc"
export XAUTHORITY="${XDG_RUNTIME_DIR}/Xauthority"

# wayland
export WLR_NO_HARDWARE_CURSORS=1

# compiler
export CARGO_HOME="${XDG_DATA_HOME}/cargo"
export RUSTUP_HOME="${XDG_DATA_HOME}/rustup"
export GOPATH="${XDG_DATA_HOME}/go"

# shinit
export ENV="${HOME}/.config/init.sh"
export EDITOR="open.sh"
export VISUAL="$EDITOR"
export OPEN="$EDITOR"
export OPENER="$EDITOR"
export BROWSER="$EDITOR"

# misc
export TUIR_BROWSER="$EDITOR"
export GNUPGHOME="${XDG_DATA_HOME}/gnupg"
export WINEPREFIX="${XDG_DATA_HOME}/wineprefixes/default"
export ABDUCO_SOCKET_DIR="${XDG_DATA_HOME}/"
export LESSHISTFILE="/dev/null"
export EXINIT="set noflash"

# PATH
append_path "${HOME}/.local/bin"
append_path "${XDG_DATA_HOME}/cargo/bin"
append_path "${XDG_DATA_HOME}/go/bin"
append_path "${XDG_DATA_HOME}/flatpak/exports/bin"
append_path "${XDG_DATA_HOME}/game"
append_path "/var/lib/flatpak/exports/bin"
append_path "/usr/local/sbin"
append_path "/usr/local/bin"
append_path "/usr/bin"
append_path "."
